({
    navHome : function(object) {
        var evento = $A.get("e.c:redirect");
        evento.setParams({"data":object});
        evento.fire();
    },
    situacCodigosFavoritos : function(lista, component, event, helper){
        var action = component.get("c.grabarCodigosFavoritos");
        action.setParams({
            listaIds : lista
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() != null && response.getReturnValue() != undefined){
                    debugger;
                    var wrapper = response.getReturnValue();
                    component.set('v.codigosFavoritos', response.getReturnValue());
                    console.log('FlowCodigoFavoritoCodImput-situacCodigosFavoritos-response.getReturnValue()',response.getReturnValue());
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "message":  wrapper.message,
                        "title" : wrapper.error,
                    });
                    toastEvent.fire();
                }
                
                this.navHome(component.get('v.objectApiName'));
            } 
        });       
        $A.enqueueAction(action);
    }, 
    
})