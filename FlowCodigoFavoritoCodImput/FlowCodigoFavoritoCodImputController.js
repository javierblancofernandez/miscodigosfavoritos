({
    doInit : function(component, event, helper) {
        //codigos seleccionados
        var accIds = component.get("v.selected");
        var selectedObject = component.get("v.objectApiName");
        if (accIds != undefined && accIds != null && accIds != ''){
            var accIdsSliced = accIds.slice(1, accIds.length - 1);
            var arrayCodigosImput = accIdsSliced.split(', ');
            component.set('v.listaCodigosImputables', arrayCodigosImput);
            console.log('esto son codigos imputables :',arrayCodigosImput);
            console.log('este es el objeto seleccionado :',selectedObject);
            helper.situacCodigosFavoritos(arrayCodigosImput, component, event, helper);

        }else{
            $A.enqueueAction(component.get('c.errorNoCodImput'));
            
        }
    },
    
    errorNoCodImput : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": "errorNoCodImput"
        });
        toastEvent.fire();
        helper.navHome(component.get('v.objectApiName'));
    },  
    
})