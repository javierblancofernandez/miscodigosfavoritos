public class NB_FlowCodigoFavorito_controller {
    //Creado por: Javier Blanco 18/09/2020
    //Modificado por:
    //FUNCIÓN DEL METODO: Recoge los Ids de la list view(Codigos Imputables,Proyectos o Oportunidades) y graba los registros de Codigos Imputables en Mis Códigos Favoritos.
    //Parámetros de entrada: Lista de Ids seleccionados
    //Parámetro de  salida:	WrapperResponse.
    //100. Recuperar ids seleccionados en la list view, que nos pasa el componente aura (puede ser proyecto, oportunidad o codigo imputable).
    //101. Recuperar los codigos imputables y comprobar que tienen situacion cerrado false,y admite horas o gastos true.Si no da error. 
    //102. Comprobar que el recurso personal tiene un usuario de Salesforce y grabar en codigos favoritos.
    @AuraEnabled
    public static WrapperResponse grabarCodigosFavoritos(List<String> listaIds){
        
        List<NB_CRP_RecursosPersonales__c> recursoPersonall = new List<NB_CRP_RecursosPersonales__c>();
        WrapperResponse responseCodFav = new WrapperResponse ( false , 'Codigos Favoritos actualizados');
        // Método para filtrar según sea Oportunidad, Proyecto o Código Imputable y conseguir los códigos imputables.
        List<NB_CCI_CodigoImputable__c> codImputables = getListaCodigosImputables(listaIds);
        
        // Id de Recurso Personal.
        String usuario = userInfo.getUserId();
        
        // Comprobamos que los Codigós Imputables tengan Situación Cerrado = True y Admitan Horas ó Admitan Gastos.
        for(NB_CCI_CodigoImputable__c codImputab : codImputables){
            
            if(codImputab.NB_CCI_CH_SituacionCerrado__c == True){
                responseCodFav.error = True;
                responseCodFav.message = 'Error - No se pueden marcar Códigos Imputables cerrados.';
            }else if(codImputab.NB_CCI_CH_AdmiteHoras__c == False && codImputab.NB_CCI_CH_AdmiteGastos__c == False){
                responseCodFav.error = True;
                responseCodFav.message = 'Error - Se tienen que marcar Códigos Imputables que admitan horas o gastos';   
            }
        }
        
        // Guardamos Codigos Imputables en Mis Codigos Favoritos.
        if(responseCodFav.error == false){
            // Comprobamos que el Recurso Personal tenga Usuario de SalesForce.
            recursoPersonall =[SELECT id
                               FROM NB_CRP_RecursosPersonales__c 
                               WHERE NB_CRP_LU_UsuarioSalesforce__c=:usuario LIMIT 1 ];
            if(recursoPersonall.isEmpty()){
                responseCodFav.error = True;
                responseCodFav.message = 'Error - El Recurso Personal tiene que tener asociado un Usuario de SalesForce';
            }else{
                String recursoPersonalId = String.valueOf(recursoPersonall[0].id);
                grabarCodigosImputEnFavoritos (codImputables, recursoPersonalId );
            }
            
        }
        return responseCodFav;   
    }
    //Creado por: Javier Blanco 18/09/2020
    //Modificado por:
    //FUNCIÓN DEL METODO: Recoge los Ids de la list view(Codigos Imputables,Proyectos o Oportunidades)y filtra los Ids según sea el objeto.
    //Parámetros de entrada: Lista de Ids seleccionados.
    //Parámetro de  salida:	Lista de Códigos Imputables.
    public static List<NB_CCI_CodigoImputable__c> getListaCodigosImputables(List<String> listaIds){
        List<NB_CCI_CodigoImputable__c> codImputables = new List<NB_CCI_CodigoImputable__c>();
        //Consigo el objeto , según el id.
        Id cod = listaIds.get(0);
        Schema.SObjectType objeto = cod.getsobjecttype(); 
        String s = String.valueOf(objeto);
        System.debug('Tipo de objeto'+s);
        //Filtro según el Objeto para conseguir los Códigos Imputables.
        if(s.equals( Label.NB_CLL_NB_CPR_Proyecto_c)) {
            codImputables = [ SELECT Id,NB_CCI_CH_SituacionCerrado__c,NB_CCI_CH_AdmiteHoras__c,NB_CCI_CH_AdmiteGastos__c 
                             FROM NB_CCI_CodigoImputable__c 
                             WHERE NB_CCI_LU_CodigoProyecto__c IN :listaIds ];
            
        }else if(s.equals( Label.NB_CLL_Opportunity)) {
            codImputables = [ SELECT Id,NB_CCI_CH_SituacionCerrado__c,NB_CCI_CH_AdmiteHoras__c,NB_CCI_CH_AdmiteGastos__c 
                             FROM NB_CCI_CodigoImputable__c 
                             WHERE NB_CCI_LU_CodigoOportunidad__c IN :listaIds ];
            
            
        }else{
            codImputables = [ SELECT Id,NB_CCI_CH_SituacionCerrado__c,NB_CCI_CH_AdmiteHoras__c,NB_CCI_CH_AdmiteGastos__c 
                             FROM NB_CCI_CodigoImputable__c 
                             WHERE Id IN :listaIds ];
            
        }
        return codImputables;
    }
    
    //Creado por: Javier Blanco 18/09/2020
    //Modificado por:
    //FUNCIÓN DEL METODO: Grabar Códigos Imputables en Mis Códigos Favoritos.
    //Parámetros de entrada: Lista de Codigos Imputables , Id de recurso Personal.
    //Parámetro de  salida:	void.
    public static void grabarCodigosImputEnFavoritos (List<NB_CCI_CodigoImputable__c> codImputa,String recursoPersonalId ){
        
        List<String> codImputables = new List<String>();
        for(NB_CCI_CodigoImputable__c ci : codImputa){
            codImputables.add(ci.id);
        }
        
        System.debug('************  recursoPersonalId2  ***************'+recursoPersonalId);
        List<NB_CMF_MISCODIGOSFAVORITOS__c> codFavor = [SELECT NB_CMF_MD_CodigoImputable__c 
                                                        FROM NB_CMF_MISCODIGOSFAVORITOS__c 
                                                        WHERE NB_CMF_MD_CodigoImputable__c 
                                                        IN :codImputables
                                                        AND NB_CMF_MD_RecursoPersonal__c = :recursoPersonalId ];
        if(codFavor.size()==0){
            
            for(String codImput:codImputables){
                NB_CMF_MISCODIGOSFAVORITOS__c cf=new NB_CMF_MISCODIGOSFAVORITOS__c();
                System.debug('Estoy grabandome en codigos favoritos porque no eestoy en la lista');
                //cf.Id = null;
                cf.NB_CMF_MD_CodigoImputable__c=codImput;
                cf.NB_CMF_MD_RecursoPersonal__c=recursoPersonalId;
                codFavor.add(cf);
            }
            Database.SaveResult[] results = Database.insert(codFavor, false);
            System.debug('*** NB_FlowCodigoFavorito_controller-grabarCodigosImputEnFavoritos-results : '+results);
        }else{
            
            //Obtengo Lista de codigos imputables que no estan en la Tabla de Códigos Favoritos.
            List<String> listaFiltrada = getListaFiltrada(codFavor, codImputables);
            
            
            
            // Hago el insert de los códigos imputables que no están en la lista de códigos favoritos
            if(listaFiltrada.size() != 0){
                
                for(String codImput:listaFiltrada){
                    System.debug('el codigo imputable'+codImput);
                    NB_CMF_MISCODIGOSFAVORITOS__c cf1=new NB_CMF_MISCODIGOSFAVORITOS__c();
                    //cf1.Id = null;
                    cf1.Name=codImput;
                    cf1.NB_CMF_MD_CodigoImputable__c=codImput;
                    cf1.NB_CMF_MD_RecursoPersonal__c=recursoPersonalId;
                    codFavor.add(cf1);
                }
                
                //insert(codFavor);
                Database.SaveResult[] results = Database.insert(codFavor, false);
                System.debug('*** NB_FlowCodigoFavorito_controller-grabarCodigosImputEnFavoritos-results : '+results);
                
            }	
        }
    }
    //Creado por: Javier Blanco 18/09/2020
    //Modificado por:
    //FUNCIÓN DEL METODO:  Método para filtrar los codigos imputables con códigos favoritos y que no se dupliquen los registros.
    //Parámetros de entrada: Lista de Códigos Favoritos  , Lista de Códigos Imputables.
    //Parámetro de  salida:	Lista filtrada de Códigos Imputables.
    private static List<String> getListaFiltrada(List<NB_CMF_MISCODIGOSFAVORITOS__c> codFavor,List<String> codImputables){
        
        Set<String> listaFiltradaSinDuplicidades = new Set<String>();
        System.debug('*** NB_FlowCodigoFavorito_controller-getListaFiltrada-codiDuplicado : '+listaFiltradaSinDuplicidades);
        List<String> listaAfiltrar=new List<String>();
        List<String> listaFiltrada;
        
        for(NB_CMF_MISCODIGOSFAVORITOS__c  codiDup:codFavor){
            System.debug('Estoy en codigos Duplicados');
            String codiDuplicado = (String)codiDup.NB_CMF_MD_CodigoImputable__c;
            System.debug('*** NB_FlowCodigoFavorito_controller-getListaFiltrada-codiDuplicado : '+codiDuplicado);
            
            listaAfiltrar.add(codiDuplicado);
        }
        for(String codigo:codImputables){
            
            if(!listaAfiltrar.contains(codigo)){
                listaFiltradaSinDuplicidades.add(codigo);
            }
        }
        System.debug('este es el set'+listaFiltradaSinDuplicidades);
        listaFiltrada = new List<String>(listaFiltradaSinDuplicidades);
        return listaFiltrada;
    }

    public class WrapperResponse {
        @AuraEnabled
        public Boolean error;
        @AuraEnabled
        public String message;
        
        public wrapperResponse(Boolean error,String message){
            this.error = error;
            this.message = message;
        }
    } 
}